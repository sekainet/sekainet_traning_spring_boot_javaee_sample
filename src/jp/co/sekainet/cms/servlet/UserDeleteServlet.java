package jp.co.sekainet.cms.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.sekainet.cms.dao.UserDAO;
import jp.co.sekainet.cms.entity.User;

@WebServlet("/user/delete")
public class UserDeleteServlet extends HttpServlet {

    /** serialVersionUID. */
    private static final long serialVersionUID = 8944761311372952969L;

    private UserDAO userDao = new UserDAO();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // 管理者としてログインしていない場合はログインページへ遷移.
        User loginUser = (User)request.getSession().getAttribute("loginUser");
        if (loginUser == null || !loginUser.isAdmin()) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }
        // 設定されているIDを取り出して削除を実行
        if (request.getParameter("id") != null && !request.getParameter("id").isEmpty()) {
            try {
                long userId = Long.parseLong(request.getParameter("id"));
                userDao.delete(userId);
            } catch(NumberFormatException ignore) {}
        }
        // 削除後に一覧ページを表示
        response.sendRedirect(request.getContextPath() + "/user/list");
    }

}
