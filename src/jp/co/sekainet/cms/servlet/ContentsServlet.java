package jp.co.sekainet.cms.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/contents")
public class ContentsServlet extends HttpServlet {

    /** serialVersionUID. */
    private static final long serialVersionUID = -5042424037263982657L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("contents.jsp");
        requestDispatcher.forward(request, response);
    }
}
