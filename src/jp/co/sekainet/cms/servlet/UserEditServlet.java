package jp.co.sekainet.cms.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.sekainet.cms.dao.UserDAO;
import jp.co.sekainet.cms.entity.User;

@WebServlet("/user/edit")
public class UserEditServlet extends HttpServlet {

    /** serialVersionUID. */
    private static final long serialVersionUID = 4335048583325569642L;

    private UserDAO userDao = new UserDAO();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // 管理者としてログインしていない場合はログインページへ遷移.
        User loginUser = (User)request.getSession().getAttribute("loginUser");
        if (loginUser == null || !loginUser.isAdmin()) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }
        User user = null;
        if (request.getParameter("id") != null) {
            try {
                int id = Integer.parseInt(request.getParameter("id"));
                user = userDao.findById(id);
            } catch (Exception ignore) {}
            if (user == null) {
                // 未入力か数値を指定されなかった場合はID指定なしとしてリダイレクトする.
                response.sendRedirect(request.getRequestURI());
                return;
            }
        } else {
            user = new User();
        }
        request.setAttribute("user", user);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/user_edit.jsp");
        requestDispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        User user = null;
        // バリデーション処理
        Map<String, String> validationMessages = new HashMap<>();
        // IDが指定されている場合は更新、されていない場合は追加
        if (request.getParameter("id") != null && !request.getParameter("id").isEmpty()) {
            try {
                long userId = Long.parseLong(request.getParameter("id"));
                user = userDao.findById(userId);
                if (user == null) {
                    validationMessages.put("id", "指定されたIDのユーザーは存在しないため更新できません。");
                }
            } catch(NumberFormatException e) {
                validationMessages.put("id", "IDは数値を入力して下さい。");
            }
        } else {
            user = new User();
        }
        if (request.getParameter("loginId") == null || request.getParameter("loginId").isEmpty()) {
            validationMessages.put("loginId", "ログインIDは必須です");
        } else if (request.getParameter("loginId").length() < 6) {
            validationMessages.put("loginId", "ログインIDは6文字以上で入力して下さい");
        } else if (request.getParameter("loginId").length() > 50) {
            validationMessages.put("loginId", "ログインIDは50文字以下で入力して下さい");
        }
        if (request.getParameter("password") == null || request.getParameter("password").isEmpty()) {
            validationMessages.put("password", "パスワードは必須です");
        } else if (request.getParameter("password").length() < 8) {
            validationMessages.put("password", "パスワードは8文字以上で入力して下さい");
        } else if (request.getParameter("password").length() > 100) {
            validationMessages.put("password", "パスワードは100文字以下で入力して下さい");
        }
        if (request.getParameter("name") == null || request.getParameter("name").isEmpty()) {
            validationMessages.put("name", "名前は必須です");
        } else if (request.getParameter("name").length() > 50) {
            validationMessages.put("name", "名前は50文字以下で入力して下さい");
        }
        if (request.getParameter("phonetic") != null && request.getParameter("phonetic").length() > 100) {
            validationMessages.put("phonetic", "よみがなは100文字以下で入力して下さい");
        } else if (request.getParameter("phonetic") != null &&
                !request.getParameter("phonetic").isEmpty() &&
                !request.getParameter("phonetic").matches("^[ぁ-ゞー]+$")) {
            validationMessages.put("phonetic", "よみがなはすべて平仮名で入力して下さい");
        }
        if (request.getParameter("postalCode") != null &&
                !request.getParameter("postalCode").isEmpty() &&
                !request.getParameter("postalCode").matches("\\d{3}-\\d{4}")) {
            validationMessages.put("postalCode", "郵便番号はハイフンを含めて7桁の数値で入力して下さい");
        }
        if (request.getParameter("prefectureId") != null && !request.getParameter("prefectureId").isEmpty()) {
            try {
                int prefectureId = Integer.parseInt(request.getParameter("prefectureId"));
                if (prefectureId < 0 || prefectureId > 47) {
                    validationMessages.put("prefectureId", "都道府県の選択値が不正です。");
                }
            } catch(NumberFormatException e) {
                validationMessages.put("prefectureId", "都道府県の選択値が不正です。");
            }
        }
        if (request.getParameter("city") != null &&
                request.getParameter("city").length() > 50) {
            validationMessages.put("city", "市区町村は50文字以下で入力して下さい");
        }
        if (request.getParameter("address") != null &&
                request.getParameter("address").length() > 50) {
            validationMessages.put("address", "住所は50文字以下で入力して下さい");
        }
        if (request.getParameter("telephone") != null &&
                !request.getParameter("telephone").isEmpty() &&
                !request.getParameter("telephone").matches("0\\d{1,4}-\\d{1,4}-\\d{3,4}")) {
            validationMessages.put("telephone", "電話番号ははハイフンを含めて数値で入力して下さい");
        }
        if (request.getParameter("email") != null &&
                !request.getParameter("email").isEmpty() &&
                !request.getParameter("email").matches("[^@]+@.+\\..+")) {
            validationMessages.put("email", "メールアドレスの入力値が不正です");
        }
        if (request.getParameter("note") != null && request.getParameter("note").length() > 500) {
            validationMessages.put("note", "備考は500文字以下で入力して下さい");
        }
        // パラメータをuserへ詰替
        user.setLoginId(request.getParameter("loginId"));
        user.setPassword(request.getParameter("password"));
        user.setName(request.getParameter("name"));
        user.setPhonetic(request.getParameter("phonetic"));
        user.setPostalCode(request.getParameter("postalCode"));
        if (request.getParameter("prefectureId") != null &&
                !request.getParameter("prefectureId").isEmpty()) {
            user.setPrefectureId(Integer.parseInt(request.getParameter("prefectureId")));
        }
        user.setCity(request.getParameter("city"));
        user.setAddress(request.getParameter("address"));
        user.setTelephone(request.getParameter("telephone"));
        user.setEmail(request.getParameter("email"));
        user.setNote(request.getParameter("note"));
        try {
            user.setAdmin(Boolean.parseBoolean(request.getParameter("admin")));
        } catch (Exception ignore) {}
        // バリデーションエラーがある場合は中断して画面に通知
        if (!validationMessages.isEmpty()) {
            request.setAttribute("user", user);
            request.setAttribute("message", validationMessages);
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/user_edit.jsp");
            requestDispatcher.forward(request, response);
            return;
        }
        // 挿入もしくは更新
        if (user.getId() < 1L) {
            userDao.insert(user);
        } else {
            userDao.update(user);
        }
        System.out.println(user);
        // 更新後に一覧ページを表示
        response.sendRedirect(request.getContextPath() + "/user/list");
    }

    protected User getSpecifiedUser(HttpServletRequest request) {
        try {
            int id = Integer.parseInt(request.getParameter("id"));
            return userDao.findById(id);
        } catch (Exception ignore) {
            // 未入力か数値を指定されなかった場合は何もせずnullを返す.
        }
        return null;
    }

}
