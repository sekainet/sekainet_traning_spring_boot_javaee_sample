package jp.co.sekainet.cms.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.sekainet.cms.dao.UserDAO;
import jp.co.sekainet.cms.entity.User;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    /** serialVersionUID. */
    private static final long serialVersionUID = -2736003243179290000L;

    private UserDAO userDao = new UserDAO();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("login.jsp");
        requestDispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String loginId = request.getParameter("loginId");
        String password = request.getParameter("password");
        User loginUser = userDao.findByLoginId(loginId);
        if (loginUser != null && loginUser.getPassword().equals(password)) {
            request.getSession().setAttribute("loginUser", loginUser);
            if (loginUser.isAdmin()) {
                response.sendRedirect(request.getContextPath() + "/user/list");
            } else {
                response.sendRedirect(request.getContextPath() + "/contents");
            }
            return;
        }
        request.setAttribute("message", "ログインに失敗しました。");
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("login.jsp");
        requestDispatcher.forward(request, response);
    }
}
