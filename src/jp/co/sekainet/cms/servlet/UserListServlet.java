package jp.co.sekainet.cms.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.sekainet.cms.dao.UserDAO;
import jp.co.sekainet.cms.entity.User;

@WebServlet("/user/list")
public class UserListServlet extends HttpServlet {

    /** serialVersionUID. */
    private static final long serialVersionUID = -3400232553501413955L;

    private UserDAO userDao = new UserDAO();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // 管理者としてログインしていない場合はログインページへ遷移.
        User loginUser = (User)request.getSession().getAttribute("loginUser");
        if (loginUser == null || !loginUser.isAdmin()) {
            response.sendRedirect(request.getContextPath() + "/login");
            return;
        }
        List<User> users = userDao.findAll();
        request.setAttribute("users", users);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/user_list.jsp");
        requestDispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

}
