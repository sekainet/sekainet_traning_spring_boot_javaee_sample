package jp.co.sekainet.cms.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import jp.co.sekainet.cms.entity.User;

public class UserDAO {

    private static final String JDBC_URL = "jdbc:h2:mem:cms;DB_CLOSE_DELAY=-1";
    private static final String JDBC_USER = "sa";
    private static final String JDBC_PASS = "";

    public void insert(User user) {
        StringBuilder query = new StringBuilder();
        query.append("INSERT INTO user (")
                    .append("login_id, password, name, phonetic, postal_code, prefecture_id, ")
                    .append("city, address, telephone, email, note, admin) ")
            .append("VALUES (")
                .append("'").append(user.getLoginId()).append("', ")
                .append("'").append(user.getPassword()).append("', ")
                .append("'").append(user.getName()).append("', ")
                .append("'").append(user.getPhonetic()).append("', ")
                .append("'").append(user.getPostalCode()).append("', ")
                .append("'").append(user.getPrefectureId()).append("', ")
                .append("'").append(user.getCity()).append("', ")
                .append("'").append(user.getAddress()).append("', ")
                .append("'").append(user.getTelephone()).append("', ")
                .append("'").append(user.getEmail()).append("', ")
                .append("'").append(user.getNote()).append("', ")
                .append(user.isAdmin())
            .append(")");
        Connection connection = null;
        try {
            Class.forName("org.h2.Driver");
            connection = DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASS);
            Statement statement = connection.createStatement();
            statement.execute(query.toString(), Statement.RETURN_GENERATED_KEYS);
            ResultSet generatedKeys = statement.getGeneratedKeys();
            generatedKeys.next();
            user.setId(generatedKeys.getLong(1));
            connection.commit();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void update(User user) {
        StringBuilder query = new StringBuilder();
        query.append("UPDATE user SET ")
                .append("login_id = ").append("'").append(user.getLoginId()).append("', ")
                .append("password = ").append("'").append(user.getPassword()).append("', ")
                .append("name = ").append("'").append(user.getName()).append("', ")
                .append("phonetic = ").append("'").append(user.getPhonetic()).append("', ")
                .append("postal_code = ").append("'").append(user.getPostalCode()).append("', ")
                .append("prefecture_id = ").append("'").append(user.getPrefectureId()).append("', ")
                .append("city = ").append("'").append(user.getCity()).append("', ")
                .append("address = ").append("'").append(user.getAddress()).append("', ")
                .append("telephone = ").append("'").append(user.getTelephone()).append("', ")
                .append("email = ").append("'").append(user.getEmail()).append("', ")
                .append("note = ").append("'").append(user.getNote()).append("', ")
                .append("admin = ").append(user.isAdmin())
            .append(" WHERE id = ").append(user.getId());
        Connection connection = null;
        try {
            Class.forName("org.h2.Driver");
            connection = DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASS);
            Statement statement = connection.createStatement();
            statement.executeUpdate(query.toString());
            connection.commit();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void delete(long id) {
        StringBuilder query = new StringBuilder();
        query.append("DELETE FROM user WHERE id = ").append(id);
        Connection connection = null;
        try {
            Class.forName("org.h2.Driver");
            connection = DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASS);
            Statement statement = connection.createStatement();
            statement.executeUpdate(query.toString());
            connection.commit();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public User findById(long id) {
        User user = null;
        Connection connection = null;
        try {
            Class.forName("org.h2.Driver");
            connection = DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASS);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from user where id = " + id);
            if (resultSet.next()) {
                user = fetchUserFromResultSet(resultSet);
            }
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return user;
    }

    public User findByLoginId(String loginId) {
        User user = null;
        Connection connection = null;
        try {
            Class.forName("org.h2.Driver");
            connection = DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASS);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(
                    "select * from user where login_id = '" + loginId + "'");
            if (resultSet.next()) {
                user = fetchUserFromResultSet(resultSet);
            }
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return user;
    }

    public List<User> findAll() {
        Connection connection = null;
        List<User> users = new ArrayList<>();
        try {
            Class.forName("org.h2.Driver");
            connection = DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASS);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from user");
            while (resultSet.next()) {
                users.add(fetchUserFromResultSet(resultSet));
            }
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return users;
    }

    protected User fetchUserFromResultSet(ResultSet resultSet) throws SQLException {
        User user;
        user = new User();
        user.setId(resultSet.getLong("id"));
        user.setLoginId(resultSet.getString("login_id"));
        user.setPassword(resultSet.getString("password"));
        user.setName(resultSet.getString("name"));
        user.setPhonetic(resultSet.getString("phonetic"));
        user.setPostalCode(resultSet.getString("postal_code"));
        user.setPrefectureId(resultSet.getInt("prefecture_id"));
        user.setCity(resultSet.getString("city"));
        user.setAddress(resultSet.getString("address"));
        user.setTelephone(resultSet.getString("telephone"));
        user.setEmail(resultSet.getString("email"));
        user.setNote(resultSet.getString("note"));
        user.setAdmin(resultSet.getBoolean("admin"));
        return user;
    }
}
