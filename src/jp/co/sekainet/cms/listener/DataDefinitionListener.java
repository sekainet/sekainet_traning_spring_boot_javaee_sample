package jp.co.sekainet.cms.listener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import jp.co.sekainet.cms.dao.UserDAO;

@WebListener
public class DataDefinitionListener implements ServletContextListener {

    private static final String JDBC_URL = "jdbc:h2:mem:cms;DB_CLOSE_DELAY=-1";
    private static final String JDBC_USER = "sa";
    private static final String JDBC_PASS = "";

    @Override
    public void contextInitialized(ServletContextEvent event) {
        System.out.println(System.getProperty("user.dir"));
        String schemaSql = readSchemaSql();
        System.out.println("DDL定義");
        System.out.println(schemaSql);
        execute(schemaSql);
        System.out.println(new UserDAO().findAll());
    }

    protected String readSchemaSql() {
        StringBuilder schemaSql = new StringBuilder();
        try (BufferedReader schemaSqlReader = new BufferedReader(
                new InputStreamReader(
                        this.getClass().getResourceAsStream("/ddl/schema.sql"), "UTF-8"))) {
            String text;
            while ((text = schemaSqlReader.readLine()) != null) {
                schemaSql.append(text).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return schemaSql.toString();
    }

    protected void execute(String schemaSql) {
        Connection connection = null;
        try {
            Class.forName("org.h2.Driver");
            connection = DriverManager.getConnection(JDBC_URL, JDBC_USER, JDBC_PASS);
            System.out.println(connection);
            Statement statement = connection.createStatement();
            statement.executeUpdate(schemaSql);
            connection.commit();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
    }

}
