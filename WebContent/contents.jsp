<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>顧客管理システム:コンテンツ</title>
<style type="text/css">
span.error { color: red; }
</style>
</head>
<body>
  <p>${loginUser.name}さん <a href="./logout">ログアウト</a></p>
  <p>コンテンツがここに表示される。</p>
</body>
</html>