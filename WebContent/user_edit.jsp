<%@ page import="jp.co.sekainet.cms.entity.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>顧客管理システム:ユーザー編集</title>
<style type="text/css">
span.error { color: red; }
</style>
</head>
<body>
  <p>${loginUser.name}さん <a href="../logout">ログアウト</a></p>
  ユーザーの登録
  <form action="./edit${user.id == 0 ? '' : '?id='.concat(user.id)}" method="post">
    <p>ID: <input name="id" type="text" readonly value="${user.id == 0 ? '' : user.id}">
      <span class="error">${message.id}</span>
    </p>
    <p>ログインID: <input name="loginId" type="text" value="${user.loginId}">
      <span class="error">${message.loginId}</span>
    </p>
    <p>パスワード: <input name="password" type="password" value="${user.password}">
      <span class="error">${message.password}</span>
    </p>
    <p>名称: <input name="name" type="text" value="${user.name}">
      <span class="error">${message.name}</span>
    </p>
    <p>よみがな: <input name="phonetic" type="text" value="${user.phonetic }">
      <span class="error">${message.phonetic}</span>
    </p>
    <p>郵便番号: <input name="postalCode" type="text" value="${user.postalCode}">
      <span class="error">${message.postalCode}</span>
    </p>
    <p>都道府県: <select name="prefectureId" >
      <optgroup label="未選択">
        <option value="0" label="未選択" ${user.prefectureId == 0 ? 'selected' : ''}>
      </optgroup>
      <optgroup label="北海道・東北">
        <option value="1" label="北海道" ${user.prefectureId == 1 ? 'selected' : ''}>
        <option value="2" label="青森県" ${user.prefectureId == 2 ? 'selected' : ''}>
        <option value="3" label="岩手県" ${user.prefectureId == 3 ? 'selected' : ''}>
        <option value="4" label="宮城県" ${user.prefectureId == 4 ? 'selected' : ''}>
        <option value="5" label="秋田県" ${user.prefectureId == 5 ? 'selected' : ''}>
        <option value="6" label="山形県" ${user.prefectureId == 6 ? 'selected' : ''}>
        <option value="7" label="福島県" ${user.prefectureId == 7 ? 'selected' : ''}>
      </optgroup>
      <optgroup label="関東">
        <option value="8" label="茨城県" ${user.prefectureId == 8 ? 'selected' : ''}>
        <option value="9" label="栃木県" ${user.prefectureId == 9 ? 'selected' : ''}>
        <option value="10" label="群馬県" ${user.prefectureId == 10 ? 'selected' : ''}>
        <option value="11" label="埼玉県" ${user.prefectureId == 11 ? 'selected' : ''}>
        <option value="12" label="千葉県" ${user.prefectureId == 12 ? 'selected' : ''}>
        <option value="13" label="東京都" ${user.prefectureId == 13 ? 'selected' : ''}>
        <option value="14" label="神奈川県" ${user.prefectureId == 14 ? 'selected' : ''}>
      </optgroup>
      <optgroup label="中部">
        <option value="15" label="新潟県" ${user.prefectureId == 15 ? 'selected' : ''}>
        <option value="16" label="富山県" ${user.prefectureId == 16 ? 'selected' : ''}>
        <option value="17" label="石川県" ${user.prefectureId == 17 ? 'selected' : ''}>
        <option value="18" label="福井県" ${user.prefectureId == 18 ? 'selected' : ''}>
        <option value="19" label="山梨県" ${user.prefectureId == 19 ? 'selected' : ''}>
        <option value="20" label="長野県" ${user.prefectureId == 20 ? 'selected' : ''}>
        <option value="21" label="岐阜県" ${user.prefectureId == 21 ? 'selected' : ''}>
        <option value="22" label="静岡県" ${user.prefectureId == 22 ? 'selected' : ''}>
        <option value="23" label="愛知県" ${user.prefectureId == 23 ? 'selected' : ''}>
      </optgroup>
      <optgroup label="関西">
        <option value="24" label="三重県" ${user.prefectureId == 24 ? 'selected' : ''}>
        <option value="25" label="滋賀県" ${user.prefectureId == 25 ? 'selected' : ''}>
        <option value="26" label="京都府" ${user.prefectureId == 26 ? 'selected' : ''}>
        <option value="27" label="大阪府" ${user.prefectureId == 27 ? 'selected' : ''}>
        <option value="28" label="兵庫県" ${user.prefectureId == 28 ? 'selected' : ''}>
        <option value="29" label="奈良県" ${user.prefectureId == 29 ? 'selected' : ''}>
        <option value="30" label="和歌山県" ${user.prefectureId == 30 ? 'selected' : ''}>
      </optgroup>
      <optgroup label="中国">
        <option value="31" label="鳥取県" ${user.prefectureId == 31 ? 'selected' : ''}>
        <option value="32" label="島根県" ${user.prefectureId == 32 ? 'selected' : ''}>
        <option value="33" label="岡山県" ${user.prefectureId == 33 ? 'selected' : ''}>
        <option value="34" label="広島県" ${user.prefectureId == 34 ? 'selected' : ''}>
        <option value="35" label="山口県" ${user.prefectureId == 35 ? 'selected' : ''}>
      </optgroup>
      <optgroup label="四国">
        <option value="36" label="徳島県" ${user.prefectureId == 36 ? 'selected' : ''}>
        <option value="37" label="香川県" ${user.prefectureId == 37 ? 'selected' : ''}>
        <option value="38" label="愛媛県" ${user.prefectureId == 38 ? 'selected' : ''}>
        <option value="39" label="高知県" ${user.prefectureId == 39 ? 'selected' : ''}>
      </optgroup>
      <optgroup label="九州・沖縄">
        <option value="40" label="福岡県" ${user.prefectureId == 40 ? 'selected' : ''}>
        <option value="41" label="佐賀県" ${user.prefectureId == 41 ? 'selected' : ''}>
        <option value="42" label="長崎県" ${user.prefectureId == 42 ? 'selected' : ''}>
        <option value="43" label="熊本県" ${user.prefectureId == 43 ? 'selected' : ''}>
        <option value="44" label="大分県" ${user.prefectureId == 44 ? 'selected' : ''}>
        <option value="45" label="宮崎県" ${user.prefectureId == 45 ? 'selected' : ''}>
        <option value="46" label="鹿児島県" ${user.prefectureId == 46 ? 'selected' : ''}>
        <option value="47" label="沖縄県" ${user.prefectureId == 47 ? 'selected' : ''}>
      </optgroup>
    </select>
      <span class="error">${message.prefectureId}</span>
    </p>
    <p>市区町村: <input name="city" type="text" value="${user.city}">
      <span class="error">${message.city}</span>
    </p>
    <p>番地・建物: <input name="address" type="text" value="${user.address}">
      <span class="error">${message.address}</span>
    </p>
    <p>電話番号: <input name="telephone" type="tel" value="${user.telephone}">
      <span class="error">${message.telephone}</span>
    </p>
    <p>メールアドレス: <input name="email" type="email" value="${user.email}">
      <span class="error">${message.email}</span>
    </p>
    <p>備考: <textarea name="note" rows=”10″ cols=”60″>${user.note}</textarea>
      <span class="error">${message.note}</span>
    </p>
    <p>管理者: <input name="admin" type="checkbox" ${user.admin ? 'checked' : ''} value="true">
      <span class="error">${message.admin}</span>
    </p>
    <p>
      <input type="submit" value="送信">
      <a href="./list">一覧へ戻る</a>
    </p>
  </form>
</body>
</html>