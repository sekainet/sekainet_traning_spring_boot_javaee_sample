<%@ page import="jp.co.sekainet.cms.entity.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>顧客管理システム:ユーザー一覧</title>
<style type="text/css">
span.error { color: red; }
</style>
</head>
<body>
  <p>${loginUser.name}さん <a href="../logout">ログアウト</a></p>
  <a href="./edit">新規作成</a>
  <table border="1">
    <tr>
      <th>ID</th>
      <th>ログインID</th>
      <th>名前</th>
      <th>よみがな</th>
      <th>郵便番号</th>
      <th>都道府県</th>
      <th>市区町村</th>
      <th>番地・建物</th>
      <th>電話番号</th>
      <th>メールアドレス</th>
      <th>備考</th>
      <th>管理者</th>
      <th>編集</th>
      <th>削除</th>
    </tr>
    <c:forEach var="user" items="${users}" varStatus="s">
    <tr>
      <td>${user.id}</td>
      <td>${user.loginId}</td>
      <td>${user.name}</td>
      <td>${user.phonetic}</td>
      <td>${user.postalCode}</td>
      <td>${user.prefecture}</td>
      <td>${user.city}</td>
      <td>${user.address}</td>
      <td>${user.telephone}</td>
      <td>${user.email}</td>
      <td>${user.note}</td>
      <td><input type="checkbox" ${user.admin ? 'checked' : ''} onclick="return false;"></td>
      <td><a href="./edit?id=${user.id}">編集</a></td>
      <td><a href="./delete?id=${user.id}"
            onclick="return window.confirm('「${user.name}」を削除します。よろしいですか？');">削除</a></td>
    </tr>
    </c:forEach>
  </table>
</body>
</html>