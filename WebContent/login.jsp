<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>顧客管理システム:ログイン</title>
<style type="text/css">
span.error { color: red; }
</style>
</head>
<body>
  <form action="./login" method="post">
    <p>顧客管理システムへようこそ<p>
    <p>ログインID: <input name="loginId" type="text"></p>
    <p>パスワード: <input name="password" type="password"></p>
    <p><span class="error">${message}</span></p>
    <p><input type="submit" value="送信"></p>
  </form>
</body>
</html>