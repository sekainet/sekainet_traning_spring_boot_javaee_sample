# はじめに #

このプロジェクトはセカイネット研修 Spring Boot編の資料となるソースコードを管理しています。
研修内で開発するSpring Bootによる開発と比較するため、フレームワークを使用せず、
Java EEの基本機能(Servlet, JSP)のみで記述されています。

### セットアップ方法 ###

Eclipse ([Pleiades All in One](http://mergedoc.osdn.jp/) の使用をおすすめします)プロジェクトとしてインポートしてください。
その際、EclipseのサーバーにTomcat v7.0がセットアップされている必要があります。[参考手順](http://qiita.com/deonathg/items/6bae10ccfe69207a18fc)

### 権利・帰属 ###

このソースコードの権利は株式会社セカイネットが有します。
このソースコードの利用に関しては、研修資料と同様です。（常識的な範囲内での再利用や改変を認めます）
お問い合わせは (contact@sekainet.co.jp)[mailto:contact@sekainet.co.jp] までお問い合わせください。